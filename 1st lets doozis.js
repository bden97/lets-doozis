import normalizeHtml, {normalizeHtmlAttribute} from "./normalizeHtml"
describe ("normalizeHtml", function(){
    it("Converts & to &amp'", function (){
        expect (normalizeHtml("&")).toBe("&amp;")
    })
        it("Converts &raquo; to »", function (){
            expect (normalizeHtml ("&raquo; ")) . toBe ("»")
        })
    })
//just copied from network
